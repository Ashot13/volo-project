import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoginService } from './../services/login.service';

@Injectable()
export class ActiveUserGuard implements CanActivate {

  constructor(private route:Router, private loginService:LoginService){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){  
    if(this.loginService.token){      
      return true;
    }else{
      this.route.navigate(['not-loged-in']);
      return false;
    }
  }
}
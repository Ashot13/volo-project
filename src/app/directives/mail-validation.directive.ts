import { Directive, ElementRef, Input } from '@angular/core';
import { Validator, AbstractControl, Validators, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appMailValidation]',
  providers: [{provide: NG_VALIDATORS, useExisting: MailValidationDirective, multi: true}]
})
export class MailValidationDirective implements Validator{

   validate(control: AbstractControl){
    let pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return pattern.test(control.value) ? null : { 'appMailValidation' : {value: control.value}};
  }

}

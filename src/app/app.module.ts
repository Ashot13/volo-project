// Core modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/users/users.component';
import { NotLogedInUserComponent } from './components/not-loged-in-user/not-loged-in-user.component';
import { EditUserComponent } from './components/users/edit-user/edit-user.component';
import { DeleteUserComponent } from './components/users/delete-user/delete-user.component';
import { AddUserComponent } from './components/users/add-user/add-user.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';

// Services
import { LoginService } from './services/login.service';
import { UsersService } from './services/users.service';

// Guard
import { ActiveUserGuard } from './guards/active-user.guard';

// Directives
import { PasswordValidationDirective } from './directives/password-validation.directive';
import { MailValidationDirective } from './directives/mail-validation.directive';

// Routes
const appRoutes: Routes = [  
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },  
  { path: 'dashboard', component: DashboardComponent, canActivate:[ActiveUserGuard]},
  { path: 'dashboard/edit-user/:id', component: EditUserComponent, canActivate:[ActiveUserGuard]},  
  { path: 'not-loged-in', component: NotLogedInUserComponent},
  { path: '**', component: NotFoundPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,    
    UsersComponent,
    NotLogedInUserComponent,
    EditUserComponent,    
    PasswordValidationDirective,
    MailValidationDirective,
    DeleteUserComponent,
    AddUserComponent,
    NotFoundPageComponent
  ],
  imports: [
    HttpModule ,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes      
    )    
  ],
  providers: [
    LoginService,    
    UsersService,
    ActiveUserGuard    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
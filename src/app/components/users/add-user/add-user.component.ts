import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsersService } from '../../../services/users.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  submitMessage: string;
  userAdded:boolean = false;

  constructor(private addUserService:UsersService) {}

  @Output() newUsers = new EventEmitter();

  addNewUser(f: NgForm){
    let firstName = f.value.addUserForm.userFirstName;
    let lastName = f.value.addUserForm.userLastName;      
    this.addUserService.addUserData(firstName,lastName)
    .subscribe(
      response => {
        this.newUsers.emit(response);   
        f.reset(); 
        this.userAdded = true;
        this.submitMessage = response.first_name + ' ' + response.last_name;    
      },
      error => {
        console.log(error);        
      }
    );
  }

  resetForm(){
    this.userAdded = false;
  }

  ngOnInit() {
  }

}

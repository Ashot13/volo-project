import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../../services/users.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  editUserId: number;
  singleUser: Object = {};
  successMessage: Object = {};
  userEdited: boolean = false;

  constructor(private activateRoute: ActivatedRoute, private getSingleUser: UsersService, private router: Router ) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(
      params => {
        this.editUserId = params.id;
        this.getSingleUser.getSingleUser(this.editUserId)
        .subscribe(
          result => {            
            this.singleUser = result.data;                    
          }
        )        
      }
    );
  }

  edit(){
    this.getSingleUser.updateUser(this.singleUser,this.editUserId)
    .subscribe(
      result => {        
        this.successMessage = this.singleUser;
        this.userEdited = true;
      }
    );
  }
  
  back(){
    this.router.navigate(['dashboard']);
  }
}

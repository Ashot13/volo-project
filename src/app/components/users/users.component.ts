import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']  
})

export class UsersComponent implements OnInit {  
  paginationPages = [];
  activeUsers;
  pageNumber = 0; 
  totalUsers;  
  deleteUserID; 
  sort: number = -1;
  
  constructor( private getUsers: UsersService ) {}  

  @Input() confirmUserDelete: boolean = false

  ngOnInit() {
    this.getUsers.loadUsers(1).subscribe(
      result => {        
        this.totalUsers = result.total;        
        this.paginationBuilder(this.totalUsers);   
        this.activeUsers = result.data;
      }
    );   
  } 
  
  paginationBuilder(totalPagesNumber){    
    this.paginationPages=[]
    for (var index = 0; index < totalPagesNumber; index++) {
      if(index % 3 == 0){
        this.pageNumber += 1;
        this.paginationPages.push(this.pageNumber);
      }      
    }  
  }  

  getUsersList(pageNumber){
    this.getUsers.loadUsers(pageNumber).subscribe(
      result => {
        this.activeUsers = result.data;
      }
    );
  }  

  userSort(prop,sort) {
    return function(a, b) {
      if(a[prop] > b[prop]){
        return sort
      }else if(a[prop] < b[prop] ){
        return -sort
      }        
      return 0;
    }
  }  

  // TODO create with pipe
  sortBy(property){
    this.activeUsers.sort(this.userSort(property,this.sort));
    this.sort = -this.sort;  
  }

  handleNewUser(response){
    return response.first_name + ' ' + response.last_name    
  }

  deleteUser(loadedUser){    
    this.deleteUserID = loadedUser.id;    
  }

  deleteConfirmedUser($event){
    if($event = true){
      this.getUsers.deleteUser(this.deleteUserID).subscribe(
        response => {          
          for (var index = 0; index < this.activeUsers.length; index++) {
            if(this.activeUsers[index].id == this.deleteUserID){
              this.activeUsers.splice(index,1);
            }
          }
        }
      );
    }
  }
}


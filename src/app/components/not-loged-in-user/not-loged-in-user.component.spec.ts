import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotLogedInUserComponent } from './not-loged-in-user.component';

describe('NotLogedInUserComponent', () => {
  let component: NotLogedInUserComponent;
  let fixture: ComponentFixture<NotLogedInUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotLogedInUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotLogedInUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']  
})
export class LoginComponent implements OnInit{ 

  constructor(private loginservice:LoginService, private router: Router){}   

  submit(f){
    let email = f.value.loginForm.userMail;
    let password = f.value.loginForm.userPassword;        
    this.loginservice.login(email,password)
    .subscribe(
      response => {
        this.router.navigate(['dashboard']);        
      },
      error => {              
        alert('Autentification error');
      }
    )
  }

  ngOnInit(){

  }
  
}
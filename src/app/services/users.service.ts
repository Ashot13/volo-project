import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {
  private api_url = 'https://reqres.in/api/users/';  
  private headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

  constructor(private http:Http) { }

  addUserData(first_name:string, last_name:string){
    return this.http.post(
      this.api_url,
      JSON.stringify({ first_name, last_name }),
      { headers: this.headers }
    ).map(
      (response: Response) => {
        return response.json();
      }
    );
  }

  getSingleUser(userID){
    return this.http.get(
      this.api_url + userID
    ).map(
      res => res.json()
    );
  }

  loadUsers(pageNumber){
    return this.http.get(
      this.api_url + '?page=' + pageNumber
    ).map(
      res => res.json()
    );
  }

  updateUser(user, userID){
    return this.http.patch(
      this.api_url + userID, JSON.stringify(user)
    ).map(
      res => res.json()
    );
  }
 
  deleteUser(userID) {
    return this.http.delete(
      this.api_url + userID
    );
  }

}

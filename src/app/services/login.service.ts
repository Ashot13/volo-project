import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
  token: string;  
  private url = 'https://reqres.in/api/login';  
  private headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

  constructor(private http: Http) {
    this.token = localStorage.getItem('activeUser');
  }  

  login(email: string, password: string){
    return this.http.post(
      this.url,
      JSON.stringify({ email: email, password: password }),
      { headers: this.headers }
    ).map(
      (response: Response) => {
        this.token = response.json().token;  
        localStorage.setItem('activeUser', JSON.stringify({ token: this.token }));
      }
    );
  }
}

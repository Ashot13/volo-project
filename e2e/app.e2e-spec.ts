import { VoloProjectPage } from './app.po';

describe('volo-project App', () => {
  let page: VoloProjectPage;

  beforeEach(() => {
    page = new VoloProjectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
